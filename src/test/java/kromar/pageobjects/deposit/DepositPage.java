package kromar.pageobjects.deposit;

import kromar.pageobjects.AbstractPage;
import kromar.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;


import java.util.concurrent.TimeoutException;

/**
 * Created by makrolik on 11.12.13.
 */
public class DepositPage extends AbstractPage {

    /**
     * Webdriver instance
     */
    private WebDriver driver;

    /**
     * Locator values
     */
    private final String DEPOSIT = "btnSubmit";
    private final String AMOUNT = "amount";
    private final String EMAIL = "email";
    private final String PAYPAL = "radio_2";
    private final String PAGETITLE = "Jackpotjoy Bingo";
    /**
     * Webelement locators
     **/
    @FindBy(id = DEPOSIT)
    private WebElement depositButton;
    @FindBy(id = AMOUNT)
    private WebElement amount;
    @FindBy(id = EMAIL)
    private WebElement email;
    @FindBy(id = PAYPAL)
    private WebElement paypal;
    /**
    * Basic constructor
    **/
    public DepositPage(WebDriver driver){
        this.driver = driver;
    }

    /**
     * Localize self oon page by checking deposit button while waiting for it to be loaded
     * @return Is deposit button displayed?
     */
    @Override
    public Boolean isAt() {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
        // define expected condition as anonymous implementation of ExpectedCondition interface
        ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>(){
            @Override
            public Boolean apply(WebDriver d){
                return Utils.isElementPresent(d, By.id(DEPOSIT));
            }
        };
        return webDriverWait.until(condition);
    }

    @Override
    public Boolean requiredElementPresent() {
        return Utils.allElementsPresent(depositButton);
    }
}
