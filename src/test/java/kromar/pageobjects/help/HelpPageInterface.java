package kromar.pageobjects.help;

import kromar.pageobjects.AbstractPage;

/**
 * Created by makrolik on 12.12.13.
 */
public interface HelpPageInterface {

    public <T extends AbstractPage> T liveChat();
}
