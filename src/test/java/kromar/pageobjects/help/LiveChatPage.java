package kromar.pageobjects.help;

import kromar.pageobjects.AbstractPage;
import kromar.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Set;

/**
 * Created by makrolik on 12.12.13.
 */
public class LiveChatPage extends AbstractPage implements LiveChatInterface{
    /**
     * Webdriver instance
     **/
    private final WebDriver driver;
    /**
     * window handles
     */
    private String masterHandle;
    private String handle;
    private Set<String> handles;
    /**
     * Registration page title
     **/
    private final String PAGETITLE = "Live Chat";
    /**
     * Locator values
     **/
    private final String FIRSTNAME = "rn_TextInput_10_first_name";
    private final String LASTNAME ="rn_TextInput_12_last_name";
    private final String EMAIL ="rn_TextInput_14_email";
    private final String BUTTON = "rn_ChatLaunchButton_16_Button";
    /**
     * Web elements locators
     **/
    @FindBy(id = FIRSTNAME)
    private WebElement firstname;
    @FindBy(id = LASTNAME)
    private WebElement lastname;
    @FindBy(id = EMAIL)
    private WebElement email;
    @FindBy(id = BUTTON)
    private WebElement button;
    /**
     * Basic constructor
     * @param driver Webdriver instance
     **/
    public LiveChatPage(WebDriver driver){
        this.driver = driver;
        this.masterHandle = this.driver.getWindowHandle();
        this.handles = this.driver.getWindowHandles();
        for(String h: handles){
            if (!h.equals(masterHandle)){
                this.handle = h;
            }
        }
        this.driver.switchTo().window(handle);
    }
    /**
     * Localize itself on page by checking title
     * @return Does title contain "Live Chat" string?
     */
    @Override
    public Boolean isAt() {
        String actualTitle = this.driver.getTitle();
        return actualTitle.contains(PAGETITLE);
    }

    @Override
    public void contactData(String firstname, String lastname, String email) {
        this.firstname.clear();
        this.firstname.sendKeys(firstname);
        this.lastname.clear();
        this.lastname.sendKeys(lastname);
        this.email.clear();
        this.email.sendKeys(email);
    }
    /**
     * Partial implementation of submitting help request
     * @return
     */
    @Override
    public AbstractPage submit() {
        this.button.clear();
        return null;
    }

    @Override
    public Boolean requiredElementPresent() {
        return Utils.allElementsPresent(firstname, lastname, email);
    }
}
