package kromar.utils;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by makrolik on 11.12.13.
 * Utilities for Selenium framework
 */
public class Utils {

    /**
     * Element is present if it is displayed
     * @param element WebElement ot be checked
     * @return Is element present or nor?
     */
    public static Boolean isElementPresent(WebElement element){
        try{
            element.isDisplayed();
            return true;
        }
        catch (NoSuchElementException e){
            return false;
        }
    }

    /**
     * Element present if can be found using By mechanism
     * @param driver WebDriver instance
     * @param by By locator
     * @return Is element present or not?
     */
    public static Boolean isElementPresent(WebDriver driver, By by){
        try {
            driver.findElement(by);
            return true;
        }
        catch (NoSuchElementException e) {
            return false;
        }
    }

    /**
     * Element present by looking for all elements fulfilling By locator
     * @param driver WebDriver instance
     * @param by By locator
     * @return At least one such element found?
     */
    public static Boolean isElementPresentByList(WebDriver driver, By by){
        List<WebElement> list_of_elements = driver.findElements(by);
        return list_of_elements.size() >= 1;
    }

    /**
     * Any element present from list
     * @param driver WebDriver instance
     * @param args List of elements to be found
     * @return When any first element from list is found true, else false
     */
    public static Boolean anyElementPresent(WebDriver driver, By...args) {

        final List<By> byes = Arrays.asList(args);

        for (int index = 1; index < 91; index++){
            try {
                TimeUnit.SECONDS.sleep(1);
            }
            catch (InterruptedException e){
                System.out.println(e);
                System.exit(1);
            }
            for (By by: byes){
                if (Utils.isElementPresent(driver, by)){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * All elements present from list
     * @param elements list of webelements to check
     * @return True if all elements present
     */
    public static Boolean allElementsPresent(WebElement...elements){
        Boolean allPresent = true;
        for (WebElement element: elements){
            if (!Utils.isElementPresent(element)){
                allPresent = false;
            }
        }
        return allPresent;
    }

    /**
     * Converts Webelements to text strings providing webelement has Text attribute
     * @param input
     * @return Collection of text attributes of webelements
     */
    public static Collection<String> convertWebToString(Collection<WebElement> input){
        Collection<String> output = Collections2.transform(input, new Function<WebElement, String>() {
            @Nullable
            @Override
            public String apply(@Nullable WebElement webElement) {
                return webElement.getText();
            }
        });
        return output;
    }

    /**
     * Function to hack ajax content blocking selenium
     * @param driver Webdriver instance
     */
    public static void javaScriptHackToStopLoading(WebDriver driver){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("return window.stop");
    }

    public static Map<String, String> readDataFromFile(String filename){

        Map<String, String> map = new HashMap<String, String>();
        String currentLine;
        BufferedReader bufferedReader = null;

        try {
            bufferedReader = new BufferedReader(new FileReader(filename));
            int index = 0;
            while ((currentLine = bufferedReader.readLine()) != null) {
                if (!currentLine.contains("#")){
                    String[] lineArray = currentLine.split(";");
                    assert lineArray.length == 2;
                    map.put(lineArray[0], lineArray[1]);

                }
                index++;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }
}
