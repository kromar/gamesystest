package kromar.pageobjects.registration;

import kromar.pageobjects.AbstractPage;

/**
 * Created by makrolik on 11.12.13.
 * Must-have methods for RegistrationPage
 */
public interface RegistrationInterface {

    /**
     * Simulation on clicking button/submitting form
     * @return Page object representing next page in workflow
     **/
    public <T extends AbstractPage> T submit();

    /**
     * Simulation of triggering radio button
     **/
    public void acceptTerms();

}
