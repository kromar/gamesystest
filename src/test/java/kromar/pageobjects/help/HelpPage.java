package kromar.pageobjects.help;

import kromar.pageobjects.AbstractPage;
import kromar.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by makrolik on 12.12.13.
 */
public class HelpPage extends AbstractPage implements HelpPageInterface{

    /**
     * Webdriver instance
     **/
    private final WebDriver driver;

    /**
     * Help page title
     **/
    private final String PAGETITLE = "Customer Support and Help Section";

    /**
     * Locator values
     **/
    private final String LIVECHAT = "liveChat";
    /**
     * Web elements locators
     **/
    @FindBy(id = LIVECHAT)
    private WebElement liveChat;

    /**
     * Basic constructor
     * @param driver Webdriver instance
     */
    public HelpPage(WebDriver driver){
        this.driver = driver;
    }

    @Override
    public LiveChatPage liveChat() {
        this.liveChat.click();
        return PageFactory.initElements(this.driver, LiveChatPage.class);
    }

    /**
     * Localize itself on page by checking title
     * @return Does title contain "Customer Support and Help Section" string?
     */
    @Override
    public Boolean isAt() {
        String actualTitle = this.driver.getTitle();
        return actualTitle.contains(PAGETITLE);
    }

    @Override
    public Boolean requiredElementPresent() {
        return Utils.allElementsPresent(liveChat);
    }
}
