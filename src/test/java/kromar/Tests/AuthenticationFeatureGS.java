package kromar.Tests;

import kromar.SeleniumBase;
import kromar.pageobjects.home.HomePage;
import kromar.pageobjects.home.MainPage;
import kromar.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by makrolik on 12.12.13.
 * Given I'm not logged in
 * When I view the Jackpotjoy.com homepage
 * Then I should be presented with a login button
 * When I login
 * Then I should be presented with a logout button
 * When I logout
 * Then I should be on the logged out homepage to Jackpotjoy.com
 */
public class AuthenticationFeatureGS extends SeleniumBase {

    /**
     * Webdriver instance
     */
    private WebDriver driver;
    /**
     * Base url for Jakcpotjoy
     */
    private final String BASE_URL = "https://www.jackpotjoy.com/";
    /**
     * Input data from file
     */
    private Map<String, String> inputData;
    /**
     * Setups Webdriver instance
     */

    @Before
    public void setUp(){
        this.driver = loadWebDriver();
        this.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @Test
    public void loginUserAndLogout(){
        // read input data
        inputData = Utils.readDataFromFile("authentication.csv");
        //Given I’m not logged in
        //When I view the Jackpotjoy.com homepage
        this.driver.get(BASE_URL);
        HomePage home = PageFactory.initElements(this.driver, HomePage.class);
        //Then I should be presented with a login button
        assertThat("Login button", Utils.isElementPresent(this.driver.findElement(By.id("loginSubmit"))), is(true));
        //When I login
        MainPage mainPage = home.login(inputData.get("user"), inputData.get("password"));
        //Then I should be presented with a logout button
        Utils.javaScriptHackToStopLoading(this.driver);
        assertThat("Main page", mainPage.isAt(), is(true));
        //When I logout
        home = mainPage.logout();
        //Then I should be on the logged out homepage to Jackpotjoy.com
        assertThat("Home page", home.isAt(), is(true));
    }

    @After
    public void tearDown(){
        this.driver.quit();
    }
}
