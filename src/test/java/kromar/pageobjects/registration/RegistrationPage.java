package kromar.pageobjects.registration;

import kromar.pageobjects.AbstractPage;
import kromar.pageobjects.deposit.DepositPage;
import kromar.utils.Utils;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

/**
 * Created by makrolik on 11.12.13.
 *
 * Representation of Registration page
 */
public class RegistrationPage extends AbstractPage implements RegistrationInterface {
    /**
     * Webdriver instance
     **/
    private final WebDriver driver;

    /**
     * Registration page title
     **/
    private final String PAGETITLE = "I'm Loving Jackpotjoy";

    /**
    * Locator values
    **/
    private final String POSTCODE = "postcodelookup";
    private final String PHONE = "phoneNumber";
    private final String SECURITY = "securityAnswer";
    private final String TERMS = "termsAndConditions";
    private final String SUBMIT = "submit_1";

    /**
     * Web elements locators
     **/
    @FindBy(id = TERMS)
    private WebElement termsradio;
    @FindBy(id = SUBMIT)
    private WebElement submit;

    /**
     * Basic constructor
     * @param driver Webdriver instance
     **/
    public RegistrationPage(WebDriver driver){
        this.driver = driver;
        isAt();
    }

    /**
     * Clicks on submit button
     * @return NotAllowedInYourCountryPage page object
     **/
    @Override
    public DepositPage submit() {
        this.submit.click();
        DepositPage depositPage = PageFactory.initElements(this.driver, DepositPage.class);
        return depositPage;
    }

    /**
     * Clicks on Accept Terms and Conditions radio button
     **/
    @Override
    public void acceptTerms() {
        this.termsradio.click();
    }

    /**
     * Localize itself on page by checking title
     * @return Does title contain "I'm Loving Jackpotjoy" string?
     */
    @Override
    public final Boolean isAt() {
        String actualTitle = this.driver.getTitle();
        return actualTitle.contains(PAGETITLE);
    }

    @Override
    public Boolean requiredElementPresent() {
        return Utils.allElementsPresent(submit, termsradio);
    }

    /**
     * Sets up text values in login subsections
     * @param username username
     * @param password password
     * @param email email
     */
    public void setLoginSection(String username, String password, String email){
        RegistrationSubSectionLogin loginSubsection = PageFactory.initElements(this.driver, RegistrationSubSectionLogin.class);
        loginSubsection.setUsername(username);
        loginSubsection.setPassword(password);
        loginSubsection.setConfirm(password);
        loginSubsection.setEmail(email);
        loginSubsection.setConfirmEmail(email);
    }

    /**
     * Sets up values in details subsection
     * @param title users title
     * @param firstname users first name
     * @param surname users surname
     * @param day day of birth
     * @param month month of birth
     * @param year year of birth
     */
    public void setDetailsSection(String title,
                                  String firstname,
                                  String surname,
                                  String day,
                                  String month,
                                  String year){
        RegistrationSubsectionDetails detailsSubsection = PageFactory.initElements(this.driver, RegistrationSubsectionDetails.class);
        detailsSubsection.setTitle(title);
        detailsSubsection.setFirstname(firstname);
        detailsSubsection.setSurname(surname);
        detailsSubsection.setDateOfBirth(day, month, year);
    }

    /**
     * Sets up values in address subsection
     * @param postcode users postcode
     * @param phone users phone
     * @param security security question
     */
    public void setAddressSection(String postcode, String phone, String security){
        RegistrationSubsectionAddress addressSubsection = PageFactory.initElements(this.driver, RegistrationSubsectionAddress.class);
        addressSubsection.setPostcode(postcode);
        addressSubsection.setPhone(phone);
        addressSubsection.setSecurity(security);
        addressSubsection.findAddress();
    }

}
