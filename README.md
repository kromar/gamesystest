Gamesys Jackpotjoy simple framework
=======================

Test framework for Jackpotjoy.com site exercising three basic scenarios.
Framework based on PageObjects design pattern utilizing PageFactory and JUnit.
Test data separated from test scripts - stored in csv file.
Maven template for execution of Selenium tests for different browsers based on https://github.com/Ardesco/Selenium-Maven-Template

To execute all tests do the following:

1. git clone https://kromar@bitbucket.org/kromar/gamesystest.git
2. cd /path/to/gamesystest
3. mvn clean install -U -Pselenium-tests

All dependencies should be downloaded and all test cases testing specified features shall be executed.
Use one of the following switches to specify which browser to use

-Dbrowser=firefox
-Dbrowser=chrome

If no switch is defined, then Firefox is used as default browser.
No need to worry about downloading the chromedriver binaries.  This project will do that for you automatically.

COMMENTS TO EXERCISE:
1. Had to use proxy server to be able to go through registration (service not available in my country). Terrible connection speed made it very hard to work. Additionally randomly page was not being opened at all :(
2. Confirm email field in registration form	was displayed or not depending on settings of proxy. I'm not able to say if it is required or not for UK, but I'm leaving this Webelement uncommented. 
