package kromar.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import kromar.utils.Utils;
import static org.hamcrest.MatcherAssert.*;

/**
 * Created by makrolik on 11.12.13.
 * Representation of not page displayed if registration is not allowed in users country
 * */
public class NotAllowedInYourCountryPage extends AbstractPage {
    /**
     * Webdriver instance
     */
    private final WebDriver driver;

    /**
     * Locator values
     **/
    private final String MESSAGE = "message1";

    /**
     * Web elements locators
     **/
    @FindBy(id = MESSAGE)
    private WebElement message;

    /**
     * Basic constructor
     * @param driver Webdriver instance
     **/
    public NotAllowedInYourCountryPage(WebDriver driver){
        this.driver = driver;
    }

    /**
     * Localize itself by checking message element
     * @return Is message element is visible?
     **/
    @Override
    public Boolean isAt() {
        return Utils.isElementPresent(message);
    }

    @Override
    public Boolean requiredElementPresent() {
        return Utils.allElementsPresent(message);
    }
}