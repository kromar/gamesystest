package kromar.pageobjects.help;

import kromar.pageobjects.AbstractPage;

/**
 * Created by makrolik on 12.12.13.
 */
public interface LiveChatInterface {

    /**
     * Simulates entering contact data
     * @param firstname Firstname
     * @param lastname Lastname
     * @param email email
     */
    public void contactData(String firstname, String lastname, String email);

    public <T extends AbstractPage> T submit();

}
