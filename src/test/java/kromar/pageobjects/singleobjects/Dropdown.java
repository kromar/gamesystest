package kromar.pageobjects.singleobjects;

import kromar.utils.Utils;
import org.apache.xpath.operations.Bool;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.Collection;

/**
 * Created by makrolik on 12.12.13.
 */
public class Dropdown {

    private WebElement dropdown;
    private Select select;

    /**
     * Basic constructor setting up webdriver and select
     * @param driver WebDriver istance
     * @param how How to localize dropdown element by its id
     */
    public Dropdown(WebDriver driver, String how){
        this.dropdown =  driver.findElement(By.id(how));
        this.select = new Select(this.dropdown);
    }

    /**
     * Selects all options available on dropdown as text
     * @return Collection of strings representing possible options
     */
    public Collection<String> getOptionsAsText(){
        return Utils.convertWebToString(this.select.getOptions());
    }

    /**
     * Checks if all given options are available on dropdown
     * @param values List of options
     * @return Does all given options are available in dropdown?
     */
    public Boolean checkOptions(String... values){
        Collection<String> options = getOptionsAsText();
        Boolean allPresent = true;
        for (String value: values){
            if (!options.contains(value)){
                allPresent = false;
            }
        }
        return allPresent;
    }

}
