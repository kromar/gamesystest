package kromar.Tests;

import kromar.SeleniumBase;
import kromar.pageobjects.help.HelpPage;
import kromar.pageobjects.help.LiveChatPage;
import kromar.pageobjects.home.HomePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.awt.print.PageFormat;
import java.util.concurrent.TimeUnit;

/**
 * Created by makrolik on 12.12.13.
 * Given I’m on the Jackpotjoy.com Help & FAQ page
 * Then I should presented with an option to start live chat with the support team
 * When I select the live chat link
 * Then a live chat window should appear
 * And the live chat window should have a field First Name
 * And the live chat window should have a field for Last Name
 * And the live chat window should have a field for Email Address
 */
 public class LiveChatFeatureGS extends SeleniumBase {

    /**
     * Webdriver instance
     */
    private WebDriver driver;
    /**
     * Base url for Jakcpotjoy
     */
    private final String BASE_URL = "https://www.jackpotjoy.com/";
    private final String HELP_RESOURCE = "help/help";

    /**
     * Setups Webdriver instance
     */
    @Before
    public void setUp(){
        this.driver = loadWebDriver();
        this.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @Test
    public void liveChatContatcDataAvailability(){
        // Given I’m on the Jackpotjoy.com Help & FAQ page
        this.driver.get(BASE_URL);
        HomePage homePage = PageFactory.initElements(this.driver, HomePage.class);
        HelpPage helpPage = homePage.help();
        // Then I should presented with an option to start live chat with the support team
        assertThat("Live chat element", helpPage.requiredElementPresent(), is(true));
        //When I select the live chat link
        //Then a live chat window should appear
        LiveChatPage liveChatPage = helpPage.liveChat();
        //And the live chat window should have a field First Name
        //And the live chat window should have a field for Last Name
        //And the live chat window should have a field for Email Address
        assertThat("First, last and email element", liveChatPage.requiredElementPresent(), is(true));
    }

    @After
    public void tearDown(){
        this.driver.quit();
    }

}
