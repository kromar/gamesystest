package kromar.pageobjects.home;

import kromar.pageobjects.AbstractPage;
import kromar.pageobjects.help.HelpPage;
import kromar.pageobjects.help.LiveChatPage;
import kromar.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by makrolik on 11.12.13.
 */
public class HomePage extends AbstractPage implements HomePageInterface{
    /**
     * Webdriver instance
     **/
    private WebDriver driver;
    /**
     * Locator values
     **/
    private final String LOGIN = "loginSubmit";
    private final String USER = "usernameField";
    private final String PASS = "passwordField";
    private final String HELP = "Help & FAQ";
    /**
     * Web elements locators
     **/
    @FindBy(id = LOGIN)
    private WebElement loginButton;
    @FindBy(id = USER)
    private WebElement username;
    @FindBy(id = PASS)
    private WebElement password;
    @FindBy(how = How.LINK_TEXT, using = HELP)
    private WebElement helpLink;


    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    /**
     * Perform login
     * @param user Username
     * @param passwd users password
     * @return Representation of main page with user logged in
     */
    public MainPage login(String user, String passwd){
        this.username.clear();
        this.password.clear();
        this.username.sendKeys(user);
        this.password.sendKeys(passwd);
        this.loginButton.click();
        return PageFactory.initElements(this.driver, MainPage.class);
    }


    /**
     * Localize yourself by Loginbutton presence
     * @return True if login button is present, else false.
     */
    @Override
    public Boolean isAt() {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
        // define expected condition as anonymous implementation of ExpectedCondition interface
        ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>(){
            @Override
            public Boolean apply(WebDriver d){
                return Utils.isElementPresent(d, By.id(LOGIN));
            }
        };
        return webDriverWait.until(condition);
    }

    /**
     * Redirects to Help page
     * @return HelpPage page object
     */
    @Override
    public HelpPage help() {
        this.helpLink.click();
        return PageFactory.initElements(this.driver, HelpPage.class);
    }

    @Override
    public Boolean requiredElementPresent() {
        return Utils.allElementsPresent(loginButton, username, password, helpLink);
    }
}
