package kromar.pageobjects.registration;

import kromar.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by makrolik on 11.12.13.
 * Represents subsection of registration page with username/password/email data
 */
public class RegistrationSubSectionLogin {

    /**
     * Webdriver instance
     */
    private WebDriver driver;

    /**
     * Locator values
     */
    private final String USERNAME = "userName";
    private final String PASSWORD = "plainText";
    private final String CONFIRM = "confirm";
    private final String EMAIL = "email";
    private final String CONFIRMEMAIL = "confirmEmail";

    /**
     * Webelement locators
     */
    @FindBy(id = USERNAME)
    private WebElement username;
    @FindBy(id = PASSWORD)
    private WebElement password;
    @FindBy(id = CONFIRM)
    private WebElement confirm;
    @FindBy(id = EMAIL)
    private WebElement email;
    @FindBy(id = CONFIRMEMAIL)
    private WebElement confirmemail;

    /**
     * Basic constructor
     * @param driver Webdriver instance
     **/
    public RegistrationSubSectionLogin(WebDriver driver){
        this.driver = driver;
    }

    /**
     * Clears username entry and sets up new text value
     * @param text username
     */
    public void setUsername(String text) {
        this.username.clear();
        this.username.sendKeys(text);
    }
    /**
     * Clears password entry and sets up new text value
     * @param text username
     */
    public void setPassword(String text) {
        this.password.clear();
        this.password.sendKeys(text);
    }
    /**
     * Clears confirm password entry and sets up new text value
     * @param text username
     */
    public void setConfirm(String text) {
        this.confirm.clear();
        this.confirm.sendKeys(text);
    }
    /**
     * Clears email entry and sets up new text value
     * @param text username
     */
    public void setEmail(String text) {
        this.email.clear();
        this.email.sendKeys(text);
    }
    /**
     * Clears confirm email entry and sets up new text value
     * @param text username
     */
    public void setConfirmEmail(String text) {
        // in case element is not present, it happened with proxy
        if (Utils.isElementPresent(confirmemail)){
            this.confirmemail.clear();
            this.confirmemail.sendKeys(text);
        }
    }
}
