package kromar.pageobjects.home;

import kromar.pageobjects.AbstractPage;

/**
 * Created by makrolik on 12.12.13.
 * Must-have methods fof Home page
 */
public interface MainPageInterface {

    /**
     * Simulates login out to home page
     * @param <T> Type of next page in workflow
     * @return Next page in workflow
     */
    public <T extends AbstractPage> T logout();
}
