package kromar.pageobjects.home;

import kromar.pageobjects.AbstractPage;
import kromar.pageobjects.home.HomePage;
import kromar.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by makrolik on 12.12.13.
 */
public class MainPage extends AbstractPage implements MainPageInterface{
    /**
     * Webdriver instance
     **/
    private WebDriver driver;
    /**
     * Locator values
     **/
    private final String LOGOUT = "pagelink";
    /**
     * Web elements locators
     **/
    @FindBy(id = LOGOUT)
    private WebElement logout;

    /**
     * Basic constructor
     * @param driver Webdriver instance
     */
    public MainPage(WebDriver driver){
        this.driver = driver;
    }

    /**
     * Performs logout operation
     * @return Home page is returned with user logged out
     */
    public HomePage logout(){
        this.logout.click();
        return PageFactory.initElements(this.driver, HomePage.class);
    }

    /**
     * Localize yourself by Logout button presence
     * @return True if logout button is present, else false.
     */
    @Override
    public Boolean isAt() {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
        // define expected condition as anonymous implementation of ExpectedCondition interface
        ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>(){
            @Override
            public Boolean apply(WebDriver d){
                return Utils.isElementPresent(d, By.id(LOGOUT));
            }
        };
        return webDriverWait.until(condition);
    }

    @Override
    public Boolean requiredElementPresent() {
        return Utils.allElementsPresent(logout);
    }
}
