package kromar.Tests;

import kromar.SeleniumBase;
import kromar.pageobjects.deposit.DepositPage;
import kromar.pageobjects.registration.RegistrationPage;
import kromar.pageobjects.singleobjects.Dropdown;
import kromar.utils.Utils;
import org.junit.*;

import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by makrolik on 11.12.13.
 * Given I’m on the Jackpotjoy.com registration page
 * Then there should be a field for title
 * And it should contain the following options:
 * |Miss | Mrs | Ms | Mr | Dr | Prof |
 *
 * Given I’m on the Jackpotjoy.com registration page
 * When I enter valid entries in all fields
 * And I click submit
 * Then I should be on the deposit funds page
 **/

@RunWith(JUnit4.class)
public class RegistrationFeatureGS extends SeleniumBase{

    /**
     * Webdriver instance
     */
    private WebDriver driver;
    /**
     * Base url for Jakcpotjoy
     */
    private final String BASE_URL = "https://www.jackpotjoy.com/";
    /**
     * Registration page resource name
     */
    private final String REGISTRATION_RESOURCE = "/register";
    private Map<String, String> inputData;
    /**
     * Setups Webdriver instance
     */
    @Before
    public void setUp(){
        this.driver = loadWebDriver();
        this.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @Test
    public void registerTitlesAvailability(){
        //Given I’m on the Jackpotjoy.com registration page
        this.driver.get(BASE_URL + REGISTRATION_RESOURCE);
        //Then there should be a field for title
        assertThat("Title element is present", Utils.isElementPresent(this.driver, By.id("salutation")));
        //And it should contain the following options: |Miss | Mrs | Ms | Mr | Dr | Prof |
        Dropdown titles = new Dropdown(this.driver, "salutation");
        Boolean allAvailable = titles.checkOptions("Miss", "Mrs", "Ms", "Mr", "Dr", "Prof");
        assertThat("Options provided", allAvailable, is(true));
    }

    @Test
    public void registerNewUser(){
        // read input data
        inputData = Utils.readDataFromFile("registration.csv");
        //Given I’m on the Jackpotjoy.com registration page
        this.driver.get(BASE_URL + REGISTRATION_RESOURCE);
        // When I enter valid entries in all fields
        RegistrationPage registrationPage = PageFactory.initElements(this.driver, RegistrationPage.class);
        registrationPage.setLoginSection(inputData.get("user"), inputData.get("password"), inputData.get("email"));
        registrationPage.setDetailsSection(
                inputData.get("title"),
                inputData.get("firstname"),
                inputData.get("surname"),
                inputData.get("day"),
                inputData.get("month"),
                inputData.get("year"));
        registrationPage.setAddressSection(inputData.get("postcode"), inputData.get("phone"), inputData.get("security"));
        registrationPage.acceptTerms();
        //And I click submit
        DepositPage depositPage = registrationPage.submit();
        //Then I should be on the deposit funds page
        Boolean atDeposit = depositPage.isAt();
        assertThat("Deposit page", atDeposit, is(true));
    }

    @After
    public void tearDown(){
        this.driver.quit();
    }

}
