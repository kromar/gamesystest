package kromar;

import com.opera.core.systems.OperaDriver;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.*;
import java.util.*;

import static kromar.BrowserType.FIREFOX;

public class SeleniumBase {

  private static ResourceBundle _prop = ResourceBundle.getBundle("dev");
  private static BrowserType browserType;
  
  @BeforeClass
  public static void setUpTest() {
    for (BrowserType browser : BrowserType.values()) {
      if (browser.toString().toLowerCase().equals(_prop.getString("browser").toLowerCase())) {
        browserType = browser;
      }
    }
    if (browserType == null) {
      System.err.println("Unknown browser specified, defaulting to 'Firefox'...");
      browserType = FIREFOX;
    }
  }

  protected static DesiredCapabilities generateDesiredCapabilities(BrowserType capabilityType) {
    DesiredCapabilities capabilities;
    String myProxy = "62.253.249.47:8080";//"80.193.214.233:3128";

    switch (capabilityType) {
      case IE:
        capabilities = DesiredCapabilities.internetExplorer();
        capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
        capabilities.setCapability("requireWindowFocus", true);
        break;
      case OPERA:
        capabilities = DesiredCapabilities.opera();
        capabilities.setCapability("opera.arguments", "-nowin -nomail");
        break;
      case CHROME:
        capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("chrome.switches", Arrays.asList("--no-default-browser-check"));
        HashMap<String, String> chromePreferences = new HashMap<String, String>();
        chromePreferences.put("profile.password_manager_enabled", "false");
        capabilities.setCapability("chrome.prefs", chromePreferences);
        //capabilities.setCapability("chrome.switches", Arrays.asList("--proxy-server=http://62.253.249.47:8080"));
        break;
      case FIREFOX:
        capabilities = DesiredCapabilities.firefox();
        //capabilities.setCapability(CapabilityType.PROXY, new Proxy().setHttpProxy(myProxy));
        break;
      case HTMLUNIT:
      default:
        capabilities = DesiredCapabilities.htmlUnit();
        capabilities.setCapability("javascriptEnabled", "true");
    }

    return capabilities;
  }

  protected static WebDriver loadWebDriver() {
    System.out.println("Current Operating System: " + System.getProperties().getProperty("os.name"));
    System.out.println("Current Architecture: " + System.getProperties().getProperty("os.arch"));
    System.out.println("Current Browser Selection: " + browserType);

    //Load standalone executable if required
    switch (browserType) {
      case CHROME:
        if (System.getProperties().getProperty("os.arch").toLowerCase().equals("x86_64") || System.getProperties().getProperty("os.arch").toLowerCase().equals("amd64")) {
          if (System.getProperties().getProperty("os.name").toLowerCase().contains("windows")) {
            System.setProperty("webdriver.chrome.driver", _prop.getString("binaryRootFolder") + "/windows/googlechrome/64bit/2.4/chromedriver.exe");
          } else if (System.getProperties().getProperty("os.name").toLowerCase().contains("linux")) {
            System.setProperty("webdriver.chrome.driver", _prop.getString("binaryRootFolder") + "/linux/googlechrome/64bit/2.4/chromedriver");
          }
        } else {
          if (System.getProperties().getProperty("os.name").toLowerCase().contains("windows")) {
            System.setProperty("webdriver.chrome.driver", _prop.getString("binaryRootFolder") + "/windows/googlechrome/32bit/2.4/chromedriver.exe");
          } else if (System.getProperties().getProperty("os.name").toLowerCase().contains("linux")) {
            System.setProperty("webdriver.chrome.driver", _prop.getString("binaryRootFolder") + "/linux/googlechrome/32bit/2.4/chromedriver");
          }
        }
        break;
      case IE:
        if (System.getProperties().getProperty("os.arch").toLowerCase().equals("x86_64") || System.getProperties().getProperty("os.arch").toLowerCase().equals("amd64")) {
          System.setProperty("webdriver.ie.driver", _prop.getString("binaryRootFolder") + "/windows/internetexplorer/64bit/2.29.0/IEDriverServer.exe");
        } else {
          System.setProperty("webdriver.ie.driver", _prop.getString("binaryRootFolder") + "/windows/internetexplorer/32bit/2.29.0/IEDriverServer.exe");
        }
        break;
    }

    //Instantiate driver object
    switch (browserType) {
      case FIREFOX:
        return new FirefoxDriver(generateDesiredCapabilities(browserType));
      case CHROME:
        return new ChromeDriver(generateDesiredCapabilities(browserType));
      case IE:
        return new InternetExplorerDriver(generateDesiredCapabilities(browserType));
      case OPERA:
        return new OperaDriver(generateDesiredCapabilities(browserType));
      default:
        return new HtmlUnitDriver(generateDesiredCapabilities(browserType));
    }
  }
}
