package kromar.pageobjects.registration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by makrolik on 11.12.13.
 * Represents subsection of registration page with user details data
 */
public class RegistrationSubsectionDetails {
    /**
     * Webdriver instance
     */
    private WebDriver driver;

    /**
     * Locator values
     */
    private final String TITLE = "salutation";
    private final String FIRSTNAME = "firstName";
    private final String SURNAME = "surname";
    private final String DAY = "day";
    private final String MONTH = "month";
    private final String YEAR = "year";

    /**
     * Web elements locators
     **/
    @FindBy(id = TITLE)
    private WebElement title;
    @FindBy(id = FIRSTNAME)
    private WebElement firstname;
    @FindBy(id = SURNAME)
    private WebElement surname;
    @FindBy(id = DAY)
    private WebElement day;
    @FindBy(id = MONTH)
    private WebElement month;
    @FindBy(id = YEAR)
    private WebElement year;

    /**
     * Basic constructor
     * @param driver Webdriver instance
     **/
    public RegistrationSubsectionDetails(WebDriver driver){
        this.driver = driver;
    }
    /**
     * Clears title entry and sets up new text value
     * @param text username
     */
    public void setTitle(String text) {
        Select select = new Select(title);
        select.selectByVisibleText(text);
    }
    /**
     * Clears firstname entry and sets up new text value
     * @param text username
     */
    public void setFirstname(String text) {
        this.firstname.clear();
        this.firstname.sendKeys(text);
    }
    /**
     * Clears surname entry and sets up new text value
     * @param text username
     */
    public void setSurname(String text) {
        this.surname.clear();
        this.surname.sendKeys(text);
    }
    /**
     * Clears day entry and sets up new text value
     * @param day day of birth
     * @param month month of birth
     * @param year year of birth
     */
    public void setDateOfBirth(String day, String month, String year) {
        Select selectDay = new Select(this.day);
        Select selectMonth = new Select(this.month);
        Select selectYear = new Select(this.year);
        selectDay.selectByVisibleText(day);
        selectMonth.selectByVisibleText(month);
        selectYear.selectByVisibleText(year);
    }
}
