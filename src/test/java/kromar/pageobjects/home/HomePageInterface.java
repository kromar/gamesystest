package kromar.pageobjects.home;

import kromar.pageobjects.AbstractPage;

/**
 * Created by makrolik on 12.12.13.
 * Must-have methods fof Home page
 */
public interface HomePageInterface {

    /**
     * Simulates login to main page
     * @param username username
     * @param password users password
     * @param <T> Type of next page in workflow
     * @return Next page in workflow
     */
    public <T extends AbstractPage> T login(String username, String password);

    public <T extends AbstractPage> T help();
}
