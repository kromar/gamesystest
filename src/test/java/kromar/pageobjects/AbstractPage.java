package kromar.pageobjects;

import org.openqa.selenium.WebElement;

/**
 * Created by makrolik on 11.12.13.
 * Representation of general page with common implementations
 **/
public abstract class AbstractPage {
    /**
     * Checks if particular page is shown
     **/
    public abstract Boolean isAt();

    /**
     * Checks page objects required elements for presence
     * @return True if all required are present
     */
    public abstract Boolean requiredElementPresent();

}
