package kromar.pageobjects.registration;

import kromar.utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by makrolik on 11.12.13.
 */
public class RegistrationSubsectionAddress {

    /**
     * Locator values
     **/
    private final String POSTCODE = "postcodelookup";
    private final String PHONE = "phoneNumber";
    private final String SECURITY = "securityAnswer";
    private final String FINDADDRESS = "findAddressLink";
    private final String ADDRESSLINE1 = "addressLine1";
    private final String ADDRESSLINE2 = "addressLine2";

    /**
     * Web elements locators
     **/
    @FindBy(id = POSTCODE)
    private WebElement postcode;
    @FindBy(id = PHONE)
    private WebElement phone;
    @FindBy(id = SECURITY)
    private WebElement security;
    @FindBy(id = FINDADDRESS)
    private WebElement find;
    @FindBy(id = ADDRESSLINE1)
    private WebElement addressline1;
    @FindBy(id = ADDRESSLINE2)
    private WebElement addressline2;

    /**
     * Clears cpostcode entry and sets up new text value
     * @param text post code
     */
    public void setPostcode(String text) {
        this.postcode.clear();
        this.postcode.sendKeys(text);
    }
    /**
     * Clears phone number entry and sets up new text value
     * @param text phonenumber
     */
    public void setPhone(String text) {
        this.phone.clear();
        this.phone.sendKeys(text);
    }
    /**
     * Clears security question entry and sets up new text value
     * @param text security question
     */
    public void setSecurity(String text) {
        this.security.clear();
        this.security.sendKeys(text);
    }

    /**
     * Simulates clicking on find adress button
     */
    public void findAddress(){
        this.find.click();
        assertThat("Address line 1", Utils.isElementPresent(addressline1));
        assertThat("Address line 1", Utils.isElementPresent(addressline2));
    }


}
