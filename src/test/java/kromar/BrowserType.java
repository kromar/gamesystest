package kromar;

public enum BrowserType {
  FIREFOX("firefox"),
  CHROME("chrome"),
  IE("ie"),
  OPERA("opera"),
  REMOTE("remote"),
  HTMLUNIT("htmlunit");

  private final String browser;

  BrowserType(String browser) {
    this.browser = browser;
  }

  public String getBrowser() {
    return browser;
  }
}
